%global api 1.0

Name:          dleyna-server
Version:       0.6.0
Release:       6
Summary:       APIs for discovering Digital Media Servers
License:       LGPLv2
URL:           https://01.org/dleyna/
Source0:       https://01.org/sites/default/files/downloads/dleyna/dleyna-server-%{version}.tar_2.gz
Patch0:        port-to-gupnp-1.2.patch
Patch1:        dleyna-server-Avoid-possible-crash-when-getting-server-properties.patch

BuildRequires: autoconf automake libtool pkgconfig(dleyna-core-1.0) >= 0.5.0
BuildRequires: pkgconfig(gio-2.0) >= 2.28 pkgconfig(glib-2.0) >= 2.28
BuildRequires: pkgconfig(gobject-2.0) >= 2.28 pkgconfig(gssdp-1.2) >= 0.13.2
BuildRequires: pkgconfig(gupnp-1.2) >= 0.20.3 pkgconfig(gupnp-av-1.0) >= 0.11.5
BuildRequires: pkgconfig(gupnp-dlna-2.0) >= 0.9.4 pkgconfig(libsoup-2.4) >= 2.28.2
BuildRequires: chrpath
Requires:      dbus dleyna-connector-dbus

%description
Dleyna-server provides APIs for discovering Digital Media Servers
and for browsing and searching their contents.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fiv
%configure --disable-silent-rules  --disable-static
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

%install
%make_install
chrpath -d %{buildroot}/%{_libexecdir}/dleyna-server-service
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/%{name}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS COPYING ChangeLog README
%{_datadir}/dbus-1/services/com.intel.dleyna-server.service
%{_libdir}/dleyna-server/libdleyna-server-%{api}.so.*
%{_libexecdir}/dleyna-server-service
%config(noreplace) %{_sysconfdir}/dleyna-server-service.conf
%config(noreplace) /etc/ld.so.conf.d/*
%exclude %{_includedir}
%exclude %{_libdir}/dleyna-server/{libdleyna-server-1.0.la,libdleyna-server-%{api}.so}
%exclude %{_libdir}/pkgconfig

%changelog
* Wed Sep 9 2021 caodongxia <caodongxia@huawei.com> - 0.6.0-6
- Remove rpath

* Wed Jun 23 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 0.6.0-5
- Port to gupnp 1.2
- Avoid crash when getting server properties

* Thu Apr 16 2020 chengzihan <chengzihan2@huawei.com> - 0.6.0-4
- Package init
